<?php

class Gui {

    private					$_chartData;
	
	// Consctructeur de la class
	function 				__construct() {
        $this->_chartData = "";
    }
	
	public	function		displayPoint($x, $y, $nuage)
	{
		$this->_chartData .= "[" .$x. ", " .$y. "],";
	}
	
	public	function		displayChart()
	{
		echo "<script type='text/javascript'>$(function () {
				$('#container').highcharts({
					chart: {
						type: 'scatter',
						zoomType: 'xy'
					},
					title: {
						text: 'Categowit'
					},
					xAxis: {
						title: {
							enabled: true,
							text: 'Position (X)'
						},
						startOnTick: true,
						endOnTick: true,
						showLastLabel: true
					},
					yAxis: {
						title: {
							text: 'Position (Y)'
						}
					},
					legend: {
						layout: 'vertical',
						align: 'left',
						verticalAlign: 'top',
						x: 100,
						y: 70,
						floating: true,
						backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
						borderWidth: 1
					},
					plotOptions: {
						scatter: {
							marker: {
								radius: 5,
								states: {
									hover: {
										enabled: true,
										lineColor: 'rgb(100,100,100)'
									}
								}
							},
							states: {
								hover: {
									marker: {
										enabled: false
									}
								}
							},
							tooltip: {
								headerFormat: '<b>{series.name}</b><br>',
								pointFormat: '{point.x} X {point.y} Y'
							}
						}
					},
					series: [{
						name: 'Female',
						color: 'rgba(223, 83, 83, .5)',
						data: [".$this->_chartData."]

					}, {
						name: 'Male',
						color: 'rgba(119, 152, 191, .5)',
						data: [".$this->_chartData."]
					}]
				});
			});
			</script>";
	}

    
}