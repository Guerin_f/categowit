#!/usr/bin/python2.7
#!/usr/bin/env python2.7
import sys
import os 
import re
import json

def isNum(nb, error):
    try:
        int(nb)
        return True
    except:
        print "error: " + error
    return False

if (len(sys.argv) != 2):
    print "Usage: " + sys.argv[0] + " <File to show>"
    exit(1)
if not (os.path.isfile(sys.argv[1])):
    print "Error the file don't exist."
    exit(1)

points = []
#points = {'x':0,'y':1,'ad':-1,'nuage':-1}
data = {'nbNuage':0, 'points':None}

file = open(sys.argv[1],'r')

line = file.readline()

if not isNum(line, "First line should be a number."):
    exit(1)
data['nbNuage'] = int(line)

line = file.readline()
mode = 0
while line:
    line = file.readline()
    match = re.match("(\d+) (\d+)", line)
    if match:
        if mode == 0:
            points.append({'x':int(match.group(1)),'y':int(match.group(2)),'ad': 0 ,'nuage':-1})
        else:
            for point in points:
                if point['x'] == int(match.group(1)) and point['y'] == int(match.group(2)):
                    point['ad'] = 1
                    break
    elif line == "#\n":
        mode = 1
data['points'] = points
print json.dumps(data).replace('\'','"')
