<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Le python sa craint grave...</title>

    <meta name="viewport" content="width=device-width, user-scalable=yes">

    <link href="assets/font/SourceSansPro.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="gui/gui.css">

    <!-- inclusion jQuery -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="assets/css/jquery-ui.css" />

	<script type="text/javascript" src="assets/js/highcharts.js"></script>
    <!--<script type="text/javascript" src="assets/js/script.js"></script>-->
</head>